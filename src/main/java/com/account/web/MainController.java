package com.account.web;

import com.account.domain.Main;
import com.account.repository.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * Created by mac on 2016. 7. 4..
 */
@Controller
@RequestMapping("/main")
public class MainController {

    @Autowired
    private MainRepository mainRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("mains", mainRepository.findAll());
        return "main/list";
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(Model model) {
        model.addAttribute("main", new Main());
        return "main/form";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@Valid Main main, BindingResult result) {
        if (result.hasErrors()) {
            return "main/form";
        } else {
            mainRepository.save(main);
            return "redirect:list";
        }

    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public String view(@PathVariable("id") long id, Model model) {

        model.addAttribute("main", mainRepository.findOne(id));
        return "main/view";
    }

}