package com.account.web;

import com.account.domain.Main;
import com.account.domain.account.Account;
import com.account.domain.account.AccountTarget;
import com.account.domain.account.AccountType;
import com.account.repository.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by mac on 2016. 7. 4..
 */
@Controller
@RequestMapping(value = "/account")
public class AccountController {

    @Autowired
    private MainRepository mainRepository;

    @RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
    public String form(@PathVariable long id, Model model) {
        model.addAttribute("main", id);
        model.addAttribute("account", new Account());

        model.addAttribute("targets", AccountTarget.values());
        model.addAttribute("types", AccountType.values());

        return "main/account/form";
    }

    @RequestMapping(value = "/create/{mainId}", method = RequestMethod.POST)
    public String view(@PathVariable("mainId") long mainId, @Valid Account account, BindingResult result, Model model) {

        if(result.hasErrors()) {
            model.addAttribute("main", mainId);
            model.addAttribute("targets", AccountTarget.values());
            model.addAttribute("types", AccountType.values());

            return "main/account/form";
        } else {
            Main main = mainRepository.findOne(mainId);
            main.getAccounts().add(account);

            mainRepository.save(main);

            return "redirect:/main/view/{mainId}";

        }
    }

}
