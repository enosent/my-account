package com.account.service;

import com.account.domain.Main;
import com.account.repository.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mac on 2016. 7. 4..
 */
@Service
public class MainService {

    @Autowired
    private MainRepository mainRepository;

    public List<Main> findAll() {
        List<Main> list = mainRepository.findAll();
        return list;
    }

    public void save(Main main) {
        mainRepository.save(main);
    }

    public Main view(long id) {
        Main main = mainRepository.findOne(id);
        return main;
    }

}
