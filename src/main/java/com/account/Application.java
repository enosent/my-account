package com.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mac on 2016. 7. 4..
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args){

        SpringApplication.run(Application.class, args);
    }

}