package com.account.domain.account;

/**
 * Created by mac on 2016. 7. 4..
 */
public enum AccountType {

    SHINHAN("신한은행"), HANA("하나은행"), HYUNDAI_X("현대카드");

    private String desc;

    AccountType(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
