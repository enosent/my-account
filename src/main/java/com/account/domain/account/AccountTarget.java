package com.account.domain.account;

/**
 * Created by mac on 2016. 7. 4..
 */
public enum AccountTarget {

    LAUNCH("점심"),

    PC_ROOM("피씨방비"),
    WEB_CASH("웹 현질"),
    MOBILE_CASH("모바일 현질"),

    SOJU_TIME("술"),

    EAT_OUT("외식"),

    EATING("군것질"),

    MOVIE("영화"),

    TRAVEL("여행"),
    SHOPPING("쇼핑"),

    SPA("목욕, 찜질방"),

    SAVED("저금"),

    PHONE("핸드폰비"),
    WOLRD_VISION("월드비젼"),
    DEPOSIT_ACCOUNT("저축 (적금, 청약 ..etc)");

    private String desc;

    AccountTarget(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
