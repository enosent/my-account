package com.account.domain.account;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by mac on 2016. 7. 4..
 */
@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Range(min = 1, max = 31)
    private Integer day;

    @NotNull
    private Integer money;

    @NotBlank
    private String memo;

    private String etc;

    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Enumerated(EnumType.STRING)
    private AccountTarget accountTarget;

    @Temporal(TemporalType.DATE)
    private Date regDate;

}