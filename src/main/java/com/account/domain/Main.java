package com.account.domain;

import com.account.domain.account.Account;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mac on 2016. 7. 4..
 */
@Entity
@Data
@NoArgsConstructor
public class Main {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Range(min = 2015)
    private Integer year;

    @NotNull
    @Range(min = 1, max = 12)
    private Integer month;

    @NotNull
    private Integer pay;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Account> accounts = new ArrayList<Account>();

    @Temporal(TemporalType.DATE)
    private Date regDate = new Date();

}