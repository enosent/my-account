package com.account.repository;

import com.account.domain.Main;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mac on 2016. 7. 4..
 */
public interface MainRepository extends JpaRepository<Main, Long> {

}
