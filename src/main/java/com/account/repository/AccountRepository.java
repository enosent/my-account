package com.account.repository;

import com.account.domain.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mac on 2016. 7. 4..
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

}
