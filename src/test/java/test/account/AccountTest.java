package test.account;

import com.account.Application;
import com.account.domain.Main;
import com.account.domain.account.Account;
import com.account.domain.account.AccountTarget;
import com.account.domain.account.AccountType;
import com.account.repository.MainRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by mac on 2016. 7. 5..
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class AccountTest {


    @Autowired
    private MainRepository mainRepository;

    @Test
    public void findAllTest() {
        List<Main> list = mainRepository.findAll();

        Map<AccountTarget, List<Account>> usedPattern = new HashMap<>();
        Map<AccountType, List<Account>> usedTarget = new HashMap<>();
        List<Account> movies = new ArrayList<>();

        list.forEach(main -> {
            usedPattern.putAll( main.getAccounts().stream().collect(Collectors.groupingBy(Account::getAccountTarget)) );
            usedTarget.putAll( main.getAccounts().stream().collect(Collectors.groupingBy(Account::getAccountType)) );
        });

        usedPattern.forEach((k, v) -> {
            System.out.println(k.getDesc());
            v.forEach( v1 -> System.out.println("\t"+ v1 ));
        });

        usedTarget.forEach((k, v) -> {
            System.out.println(k.getDesc());
            v.forEach( v1 -> System.out.println("\t"+ v1 ));
        });

        list.forEach(main -> {
            movies.addAll( main.getAccounts().stream().filter(acc -> AccountTarget.MOVIE.equals(acc.getAccountTarget())).collect(Collectors.toList()) );
        });

        movies.forEach(System.out::println);
    }

}
